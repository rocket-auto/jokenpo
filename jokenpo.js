let eRock = document.getElementById("eRock")
let ePaper = document.getElementById("ePaper")
let eScisors = document.getElementById("eScisors")
let Rock = document.getElementById("Rock")
let Paper = document.getElementById("Paper")
let Scisors = document.getElementById("Scisors")
let reset = document.getElementById("reset")
let result = document.getElementById("result")
//pontuação
let wScore = document.getElementById("win")
let lScore = document.getElementById("loss")
//todos os botoes
let plChoices = [Rock, Paper, Scisors]
let pcChoices = [eRock, ePaper, eScisors]
//condições
let wins = [["Rock", "Scisors"],["Paper", "Rock"],["Scisors", "Paper"]]
//pontuações
let plScore = 0
let pcScore = 0
//escondendo escolhas do computador inicialmente
for (options in pcChoices){
    pcChoices[options].style.opacity = 0
}
//randomizando
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }
//desligando os botões que não o escolhido
function turnOff(a){
    for (c in plChoices){
        plChoices[c].disabled = true
    }
    for (c in plChoices){
        if (plChoices[c].id != a){
            plChoices[c].style.opacity = 0
        }
    }
}
//botão de reset
reset.onclick = function(){
    for (c in plChoices){
        pcChoices[c].style.opacity = 0
        plChoices[c].disabled = false
        plChoices[c].style.opacity = 100
    }
}
//escolha do computador
function pcChoice(){
    let choice = getRandomInt(0, 9)
    console.log(choice)
    if (choice == 0 || choice == 3 || choice == 6){
        eRock.style.opacity = 100
        return "Rock"
    }if (choice == 1 || choice == 4 || choice == 7){
        ePaper.style.opacity = 100
        return "Paper"
    }else{
        eScisors.style.opacity = 100
        return "Scisors"
    }
}
//evento de escolha
const choose = function(event){
    // result.innerHTML = ""
    let choice = event.currentTarget.id
    turnOff(choice)
    let pc = pcChoice()
    //resultado + impressão na tela
    result.appendChild(document.createTextNode(checkWin(choice, pc)))
}
//checking result
function checkWin(player, pc){
    result.innerHTML = ""
    for(cond in wins){
        if(wins[cond][0] == player && wins[cond][1] == pc){
            wScore.innerHTML = ""
            plScore++
            wScore.appendChild(document.createTextNode(plScore))
            return "WINS"
        }else if(wins[cond][0] == pc && wins[cond][1] == player){
            lScore.innerHTML = ""
            pcScore++
            lScore.appendChild(document.createTextNode(pcScore))
            return "LOSES"
        }
    }
    return "TIES"
}
//adicionando funcionalidade aos botoes
for (c in plChoices) {
    plChoices[c].addEventListener('click', choose);
}



